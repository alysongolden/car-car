import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()


from service_rest.models import AutomobileVO


def get_automobile():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    return content.get("autos")


def create_change_automobile(automobile):
    AutomobileVO.objects.update_or_create(
        import_href=automobile["href"],
        defaults={
            "vin": automobile["vin"],
            "sold": automobile["sold"],
        },
    )


def poll():
    while True:
        print("Service poller polling for data")
        try:
            automobiles = get_automobile()
            for automobile in automobiles:
                create_change_automobile(automobile)

        except Exception as e:
            print(e, file=sys.stderr)

        time.sleep(60)


if __name__ == "__main__":
    poll()
