import json
from django.http import (
    HttpResponse,
    HttpResponseNotFound,
    HttpResponseBadRequest,
    JsonResponse,
)
from django.views.decorators.http import require_http_methods
from .encoders import (
    SalespersonEncoder,
    SaleEncoder,
    CustomerEncoder,
)
from .models import AutomobileVO, Salesperson, Customer, Sale


@require_http_methods(["GET", "POST"])
def list_or_create_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return HttpResponse(
            json.dumps(salespeople, cls=SalespersonEncoder),
            content_type="application/json",
        )
    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(**data)
            return HttpResponse(
                json.dumps(salesperson, cls=SalespersonEncoder),
                content_type="application/json",
            )
        except HttpResponseBadRequest:
            return "Invalid data in request"


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(pk=id)
        salesperson.delete()
        return JsonResponse({"message": "Salesperson deleted successfully"}, status=200)
    except Salesperson.DoesNotExist:
        return HttpResponseNotFound("Salesperson not found")


@require_http_methods(["GET", "POST"])
def list_or_create_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return HttpResponse(
            json.dumps(customers, cls=CustomerEncoder), content_type="application/json"
        )
    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            customer = Customer.objects.create(**data)
            return HttpResponse(
                json.dumps(customer, cls=CustomerEncoder),
                content_type="application/json",
            )
        except HttpResponseBadRequest:
            return "Invalid data in request"


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    try:
        customer = Customer.objects.get(pk=id)
        customer.delete()
        return JsonResponse({"message": "Customer deleted successfully"}, status=200)
    except Customer.DoesNotExist:
        return HttpResponseNotFound("Customer not found")


def list_or_create_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        for sale in sales:
            sale.price = float(sale.price)
        return HttpResponse(
            json.dumps(sales, cls=SaleEncoder), content_type="application/json"
        )

    elif request.method == "POST":
        try:
            data = json.loads(request.body)
            salesperson_id = data.pop("salesperson")
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            data["salesperson"] = salesperson
            customer_id = data.pop("customer")
            customer = Customer.objects.get(pk=customer_id)
            data["customer"] = customer
            vin = data.get("automobile")
            if vin:
                automobile = AutomobileVO.objects.get(vin=vin, sold=False)
                data["automobile"] = automobile
            else:
                return HttpResponseBadRequest("Invalid automobile data")
            sale = Sale.objects.create(**data)
            automobile.sold = True
            automobile.save()
            automobile = AutomobileVO.objects.get(vin=vin)

            return HttpResponse(
                json.dumps(sale, cls=SaleEncoder), content_type="application/json"
            )
        except (
            AutomobileVO.DoesNotExist,
            KeyError,
            Salesperson.DoesNotExist,
            Customer.DoesNotExist,
        ):
            return HttpResponseBadRequest("Invalid data in request")


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    try:
        sale = Sale.objects.get(pk=id)
        automobile = sale.automobile
        automobile.sold = False
        automobile.save()
        sale.delete()
        return JsonResponse({"message": "Sale deleted successfully"}, status=200)
    except Sale.DoesNotExist:
        return HttpResponseNotFound("Sale not found")
