import React, { useState } from "react";

function CustomerForm() {
	const [formData, setFormData] = useState({
		first_name: "",
		last_name: "",
		address: "",
		phone_number: "",
	});

	const handleSubmit = async (event) => {
		event.preventDefault();

		const url = "http://localhost:8090/api/customers/new/";

		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: { "Content-Type": "application/json" },
		};

		const response = await fetch(url, fetchConfig);

		if (response.ok) {
			setFormData({
				first_name: "",
				last_name: "",
				address: "",
				phone_number: "",
			});
		} else {
			console.error("Error creating customer:", response.statusText);
		}
	};

	const handleFormChange = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value,
		});
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a Customer</h1>
					<form onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="First Name"
								required
								type="text"
								name="first_name"
								id="first_name"
								className="form-control"
								value={formData.first_name}
							/>
							<label htmlFor="first_name">First Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="Last Name"
								required
								type="text"
								name="last_name"
								id="last_name"
								className="form-control"
								value={formData.last_name}
							/>
							<label htmlFor="last_name">Last Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="Address"
								required
								type="text"
								name="address"
								id="address"
								className="form-control"
								value={formData.address}
							/>
							<label htmlFor="address">Address</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="Phone Number"
								required
								type="text"
								name="phone_number"
								id="phone_number"
								className="form-control"
								value={formData.phone_number}
							/>
							<label htmlFor="phone_number">Phone Number</label>
						</div>
						<button type="submit">Create Customer</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default CustomerForm;
