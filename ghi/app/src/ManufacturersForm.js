import React, { useState } from "react";

function ManufacturersForm() {
	const [name, setName] = useState("");
	const handleSubmit = async (event) => {
		event.preventDefault();
		const response = await fetch("http://localhost:8100/api/manufacturers/", {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify({ name }),
		});
		if (response.ok) {
			setName("");
		} else {
			console.error("Error creating manufacturer:", response.statusText);
		}
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a Manufacturer</h1>
					<form onSubmit={handleSubmit} id="new_manufacturer">
						<div className="form-floating mb-3">
							<input
								placeholder="Name"
								required
								type="test"
								name="name"
								id="name"
								className="form-control"
								value={name}
								onChange={(e) => setName(e.target.value)}
							/>
							<label htmlFor="name">Name</label>
						</div>
						<button type="submit">Create Manufacturer</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ManufacturersForm;
