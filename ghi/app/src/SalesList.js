import React, { useState, useEffect } from 'react';

function SaleList() {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    const fetchSales = async () => {
      const response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        setSales(data);
      } else {
        console.error('Error fetching sales:', response.statusText);
      }
    };
    fetchSales();
  }, []);

  return (
    <div>
      <h1>Sales</h1>
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th scope="col">Salesperson Employee ID</th>
            <th scope="col">Salesperson Name</th>
            <th scope="col">Customer</th>
            <th scope="col">VIN</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
        {sales.map((sale) => (
          <tr key={sale.automobile.id}>
            <th scope="row">{sale.salesperson.employee_id}</th>
              <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price.toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SaleList;
