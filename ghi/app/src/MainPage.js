function MainPage() {
	return (
		<div className="px-4 py-5 my-5 text-center">
			<h1 className="display-1 fw-bold">CarCar</h1>
			<div className="col-lg-6 mx-auto">
				<p className="lead mb-4">
					The premiere solution for automobile dealership management!
				</p>
				<div
					id="carouselExampleSlidesOnly"
					className="carousel slide carousel-fade"
					data-bs-ride="carousel"
				>
					<div className="carousel-inner">
						<div className="carousel-item active">
							<img
								src="https://images.pexels.com/photos/164634/pexels-photo-164634.jpeg"
								className="d-block w-100"
								alt="..."
							/>
						</div>
						<div className="carousel-item">
							<img
								src="https://images.pexels.com/photos/70912/pexels-photo-70912.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
								className="d-block w-100"
								alt="..."
							/>
						</div>
						<div className="carousel-item">
							<img
								src="https://images.pexels.com/photos/4141954/pexels-photo-4141954.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
								className="d-block w-100"
								alt="..."
							/>
						</div>
						<div className="carousel-item">
							<img
								src="https://images.pexels.com/photos/2127037/pexels-photo-2127037.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
								className="d-block w-100"
								alt="..."
							/>
						</div>
						<div className="carousel-item">
							<img
								src="https://images.pexels.com/photos/305070/pexels-photo-305070.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
								className="d-block w-100"
								alt="..."
							/>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default MainPage;
