import { useEffect, useState } from "react";

function ServiceHistoryList() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");

        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
          setFilteredAppointments(
            data.appointments.filter(
              (appointment) =>
                appointment.status === "Finished" ||
                appointment.status === "Canceled"
            )
          );
        } else {
          console.error("Failed to fetch data");
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchAppointments();
  }, []);

  return (
    <div>
      <h2>Service History</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Appointment Date</th>
            <th scope="col">Customer</th>
            <th scope="col">Vin</th>
            <th scope="col">Reason</th>
            <th scope="col">Status</th>
            <th scope="col">Technician</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.date_time}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
              <td>{appointment.technician.employee_id}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistoryList;
