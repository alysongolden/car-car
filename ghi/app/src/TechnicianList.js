import { useEffect, useState } from "react";

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnician = async () => {
      const response = await fetch("http://localhost:8080/api/technicians/");
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technician);
      } else {
        console.error("Cannot retrieve technicians:", response.statusText);
      }
    };

    fetchTechnician();
  }, []);

  return (
    <div>
      <h2>Technicians</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr key={technician.employee_id}>
              <th scope="row">{technician.employee_id}</th>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TechnicianList;
