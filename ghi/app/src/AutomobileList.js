import { useEffect, useState } from "react";

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    const fetchAutomobiles = async () => {
      const response = await fetch("http://localhost:8100/api/automobiles/");
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error("Cannot retrieve automobiles:", response.statusText);
      }
    };

    fetchAutomobiles();
  }, []);

  return (
    <div>
      <h2>Automobiles</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Vin</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Color</th>
            <th scope="col">Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => (
            <tr key={automobile.id}>
              <td>{automobile.vin}</td>
              <td>{automobile.model.manufacturer.name}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.year}</td>
              <td>{automobile.color}</td>
              <td>{automobile.sold ? "Yes" : "No"}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
