import React, { useState } from "react";

function SalespersonForm() {
	const [formData, setFormData] = useState({
		first_name: "",
		last_name: "",
		employee_id: "",
	});

	const handleSubmit = async (event) => {
		event.preventDefault();

		const url = "http://localhost:8090/api/salespeople/new/";

		const fetchConfig = {
			method: "post",
			body: JSON.stringify(formData),
			headers: { "Content-Type": "application/json" },
		};

		const response = await fetch(url, fetchConfig);

		if (response.ok) {
			setFormData({
				first_name: "",
				last_name: "",
				employee_id: "",
			});
		} else {
			console.error("Error creating salesperson:", response.statusText);
		}
	};

	const handleFormChange = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value,
		});
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a Salesperson</h1>
					<form onSubmit={handleSubmit} id="new_salesperson">
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="First Name"
								required
								type="text"
								name="first_name"
								id="first_name"
								className="form-control"
								value={formData.first_name}
							/>
							<label htmlFor="first_name">First Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="Last Name"
								required
								type="text"
								name="last_name"
								id="last_name"
								className="form-control"
								value={formData.last_name}
							/>
							<label htmlFor="last_name">Last Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleFormChange}
								placeholder="Employee ID"
								required
								type="text"
								name="employee_id"
								id="employee_id"
								className="form-control"
								value={formData.employee_id}
							/>
							<label htmlFor="employee_id">Employee ID</label>
						</div>
						<button type="submit">Create Salesperson</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default SalespersonForm;
