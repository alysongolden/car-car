import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SaleForm() {
  const [formData, setFormData] = useState({
    salesperson: '',
    customer: '',
    automobile: '',
    price: ''
  });

  const navigate = useNavigate();
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [availableAutomobiles, setAvailableAutomobiles] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [salespeopleRes, customersRes, automobilesRes] = await Promise.all([
          fetch('http://localhost:8090/api/salespeople/'),
          fetch('http://localhost:8090/api/customers/'),
          fetch('http://localhost:8100/api/automobiles/?sold=false/'),
        ]);
        if (!salespeopleRes.ok || !customersRes.ok || !automobilesRes.ok) {
          throw new Error('Error fetching data');
        }
        const [salespeopleData, customersData, automobilesData] = await Promise.all([
          salespeopleRes.json(),
          customersRes.json(),
          automobilesRes.json()
        ]);
        setSalespeople(salespeopleData);
        setCustomers(customersData);
        setAvailableAutomobiles(automobilesData.autos.filter(auto => auto.sold === false));
      } catch (error) {
        console.error('Failed to load data. Please try again.');
      }
    };
    fetchData();
  }, []);

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:8090/api/sales/new/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          salesperson: formData.salesperson,
          customer: formData.customer,
          automobile: formData.automobile,
          price: formData.price,
        }),
      });

      if (response.ok) {
        const updateResponse = await fetch(`http://localhost:8100/api/automobiles/${formData.automobile}/`, {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ sold: true })
        });

        if (updateResponse.ok) {
          setFormData({
            salesperson: '',
            customer: '',
            automobile: '',
            price: '',
          });
          navigate('/sales/');
      }
    }
  } catch (error) {
    console.error('Error in handleSubmit:', error);
  }
};

return (
  <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a Sale</h1>
        <form onSubmit={handleSubmit}>
          <div className="form-floating mb-3">
            <select name="salesperson" value={formData.salesperson} onChange={handleChange} className="form-control">
              <option value="">-- Select a Salesperson --</option>
              {salespeople.map((person) => (
                <option key={person.id} value={person.id}>
                  {person.first_name} {person.last_name}
                </option>
              ))}
            </select>
            <label htmlFor="salesperson">Select Salesperson</label>
          </div>
          <div className="form-floating mb-3">
            <select name="customer" value={formData.customer} onChange={handleChange} className="form-control">
              <option value="">-- Select a Customer --</option>
              {customers.map((customer) => (
                <option key={customer.id} value={customer.id}>
                  {customer.first_name} {customer.last_name}
                </option>
              ))}
            </select>
            <label htmlFor="customer">Select Customer</label>
          </div>
          <div className="form-floating mb-3">
            <select name="automobile" value={formData.automobile} onChange={handleChange} className="form-control">
              <option value="">-- Select an Automobile --</option>
              {availableAutomobiles.map((auto) => (
                <option key={auto.vin} value={auto.vin}>
                  {auto.year} {auto.model.manufacturer.name} {auto.model.name} ({auto.color})
                </option>
              ))}
            </select>
            <label htmlFor="automobile">Select Automobile</label>
          </div>
          <div className="form-floating mb-3">
            <input
              type="number"
              name="price"
              id="price"
              className="form-control"
              value={formData.price}
              onChange={handleChange}
              placeholder="Price"
              required
            />
            <label htmlFor="price">Price</label>
          </div>
          <button type="submit">Create Sale</button>
        </form>
      </div>
    </div>
  </div>
);
}

export default SaleForm;
