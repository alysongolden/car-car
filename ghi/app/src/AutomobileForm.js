import React, { useEffect, useState } from "react";

function AutomobileForm() {
  const [formData, setFormData] = useState({
    model_id: "",
    year: "",
    vin: "",
    color: "",
  });
  const [models, setModels] = useState([{ id: 0, name: "Gathering data" }]);

  useEffect(() => {
    const fetchModels = async () => {
      try {
        const response = await fetch("http://localhost:8100/api/models/");
        if (response.ok) {
          const data = await response.json();
          if (data && Array.isArray(data.models)) {
            setModels(data.models);
          } else {
            console.error("Models data is not in the expected format:", data);
          }
        } else {
          console.error("Failed to fetch models");
        }
      } catch (error) {
        console.error("Error fetching models:", error);
      }
    };

    fetchModels();
  }, []);

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    if (name === "model") {
      const selectedModel = models.find((model) => model.name === value);
      if (selectedModel) {
        setFormData((prevState) => ({
          ...prevState,
          model_id: selectedModel.id,
        }));
      }
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const automobileUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(automobileUrl, fetchConfig);
      if (response.ok) {
        const newAutomobile = await response.json();
        setFormData({
          model_id: "",
          year: "",
          vin: "",
          color: "",
        });
      } else if (response.status === 400) {
        const errorData = await response.json();
        console.error("Bad Request:", errorData);
        alert(errorData.message);
      } else {
        console.error("Unsuccessful, please try again:", response);
      }
    } catch (error) {
      console.error("Error:", error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create Automobile</h1>
          <form onSubmit={handleSubmit} id="automobile-form">
            <div className="form-floating mb-3">
              {models.length > 0 ? (
                <select
                  value={formData.model}
                  onChange={handleFormChange}
                  name="model"
                  id="model"
                  className="form-control"
                  required
                >
                  <option value="">Select Model</option>
                  {models.map((model) => (
                    <option key={model.id}>{model.name}</option>
                  ))}
                </select>
              ) : (
                <select
                  value={formData.model}
                  onChange={handleFormChange}
                  name="model"
                  id="model"
                  className="form-control"
                  required
                  disabled
                >
                  <option value="">Gathering data</option>
                </select>
              )}
              <label htmlFor="model">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleFormChange}
                name="vin"
                placeholder="VIN"
                type="text"
                id="vin"
                className="form-control"
                required
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.year}
                onChange={handleFormChange}
                name="year"
                placeholder="Year"
                type="text"
                id="year"
                className="form-control"
                required
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.color}
                onChange={handleFormChange}
                name="color"
                placeholder="Color"
                type="text"
                id="color"
                className="form-control"
                required
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-outline-secondary">
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
